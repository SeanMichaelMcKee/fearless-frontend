window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        const loadingTag = document.getElementById('loading-conference-spinner')
      // Here, add the 'd-none' class to the loading icon
        loadingTag.classList.add('d-none')
      // Here, remove the 'd-none' class from the select tag
        selectTag.classList.remove('d-none')
    }
});


    // Get the attendee form element by its id
    const formTag = document.getElementById("create-attendee-form");
    // Add an event handler for the submit even
    formTag.addEventListener('submit', async event => {
        // Prevent the default from happening
        event.preventDefault();

        // Create a FormData object from the form
        const formData = new FormData(formTag);
        //Get a new object from the form data's entries
        const json = JSON.stringify(Object.fromEntries(formData));

        // const selectTag = document.getElementById('conference');
        const attendeeUrl = "http://localhost:8001/api/attendees/";
        // Create options for the fetch
        // Make the fetch using the await keyword to the URL (attendeeURL)
        const response = await fetch(attendeeUrl, {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        });
        // check to see the response
        console.log(response)

        if (response.ok) {
            // reference the add and removal but instead to the successtag instead of select tag using d-none which hides it
            const successTag = document.getElementById("success-message")
            formTag.classList.add("d-none")
            successTag.classList.remove("d-none")
        }

    });

